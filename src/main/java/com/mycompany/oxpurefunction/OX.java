/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.oxpurefunction;
import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class OX {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner sc = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while(true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if(finish) {
                break;
            }
        }

    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.print("Please input row, col:");
        row = sc.nextInt();
        col = sc.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if(checkWin(table, currentPlayer, row, col)) {
                finish = true;
                showWin();
                return;
            }
            if(checkDraw()) {
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }

    }
    private static void showDraw() {
        System.out.println("Draw!!");
    }
    private static boolean checkDraw() {
        if (count==9) return true;
        return false;
    }
    private static void showWin() {
        showTable();
        System.out.println(">>>"+currentPlayer+" Win<<<");
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';  // switchPlayer()
        }else{
            currentPlayer = 'O'; 
        }
    }
    public static boolean setTable() {
        table[row-1][col-1] = currentPlayer; 
        return true;
    }

    public static boolean checkWin(char[][] table, char currentPlayer, int row, int col) {
        if(checkVertical(table, currentPlayer, col)) {
            return true;
        } else if(checkHorizontal(table, currentPlayer, row)){
            return true;
        } else if(checkX(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical(char[][] table, char currentPlayer, int col) {
        for(int r=0;r<table.length;r++) {
            if(table[r][col-1]!=currentPlayer) return false;
        }
        return true;
    }

   public static boolean checkHorizontal(char[][] table, char currentPlayer, int row) {
        for(int c=0;c<table.length;c++) {
            if(table[row-1][c]!=currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkX(char[][] table, char currentPlayer) { //Parameters
        if(checkX1(table, currentPlayer)) {
            return true;
        } else if(checkX2(table, currentPlayer)) { // Arguments
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] table, char currentPlayer) {  // 11, 22, 33
        for(int i=0;i<table.length;i++){
            if(table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table, char currentPlayer) {  // 13, 22, 31 => 02, 11, 20
        for(int i=0;i<table.length;i++){
            if(table[i][2-i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
}
