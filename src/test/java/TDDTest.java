/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class TDDTest {
    
    public TDDTest() {
    }

    //add(int a, int b) -> int
    //add(1, 2) -> 3
    @Test
    public void testAdd_1_2is3(){
        assertEquals(3, Example.add(1, 2));
    }
    //add(3,4)->7
    @Test
    public void testAdd_3_4is7(){
        assertEquals(7, Example.add(3, 4));
    }
    //add(20,22)->42
    @Test
    public void testAdd_20_22is42(){
        assertEquals(42, Example.add(20, 22));
    }
    //p: paper, s: scissors, r: rock
    //chup(char player1, char player2) -> 'p1','p2'
    @Test
    public void testChup_p1_p_p2_p_is_draw(){
        assertEquals("draw", Example.chup('p','p'));
    }
    @Test
    public void testChup_p1_r_p2_r_is_draw(){
        assertEquals("draw", Example.chup('p','p'));
    }
    
    @Test
    public void testChup_p1_s_p2_s_is_draw(){
        assertEquals("draw", Example.chup('p','p'));
    }
    
    @Test
    public void testChup_p1_s_p2_p_is_p1(){
        assertEquals("p1", Example.chup('s','p'));
    }
    
    @Test
    public void testChup_p1_r_p2_s_is_p1(){
        assertEquals("p1", Example.chup('r','s'));
    }
    
    @Test
    public void testChup_p1_p_p2_r_is_p1(){
        assertEquals("p1", Example.chup('p','r'));
    }
    
    @Test
    public void testChup_p1_s_p2_r_is_p2(){
        assertEquals("p2", Example.chup('s','r'));
    }
    
    @Test
    public void testChup_p1_p_p2_s_is_p2(){
        assertEquals("p2", Example.chup('p','s'));
    }
    
    @Test
    public void testChup_p1_r_p2_p_is_p2(){
        assertEquals("p2", Example.chup('r','p'));
    }
}
