/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.mycompany.oxpurefunction.OX;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXProgramTest {
    
    public OXProgramTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testCheckWinPlayerO() {
        char table[][] = {{'X', '-', 'O'},
                          {'-', 'X', 'O'},
                          {'O', 'X', 'O'}};
        char currentPlayer = 'O';
        int row = 2, col = 3;
        assertEquals(true, OX.checkWin(table, currentPlayer, row, col));
     }
     
     @Test
     public void testCheckWinPlayerX() {
        char table[][] = {{'X', 'X', 'X'},
                          {'O', 'X', 'O'},
                          {'O', 'X', 'O'}};
        char currentPlayer = 'X';
        int row = 1, col = 2;
        assertEquals(true, OX.checkWin(table, currentPlayer, row, col));
     }
     
     @Test
     public void testCheckHorizontalWinPlayerORow3() {
        char table[][] = {{'X', 'O', 'O'},
                          {'O', 'X', 'X'},
                          {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
     }
     
     @Test
     public void testCheckVerticalWinPlayerXCol3() {
        char table[][] = {{'X', 'O', 'X'},
                          {'O', 'X', 'X'},
                          {'O', 'O', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
     }
     
     @Test
     public void testCheckDiagonalWinPlayerO() {
        char table[][] = {{'O', 'X', 'X'},
                          {'X', 'O', 'X'},
                          {'X', 'X', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OX.checkX1(table, currentPlayer));
     }
     
     @Test
     public void testCheckDiagonalWinPlayerX() {
        char table[][] = {{'O', 'O', 'X'},
                          {'O', 'X', 'O'},
                          {'X', 'O', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, OX.checkX2(table, currentPlayer));
     }
}
