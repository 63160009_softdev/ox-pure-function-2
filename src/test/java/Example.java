
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author informatics
 */
class Example {
    static int add(int a, int b){
        return (a + b);
    }

    static String chup(char player1, char player2) {
        if(player1 == 's' && player2 == 'p'){
            return "p1";
        }else if(player1 == 'r' && player2 == 's'){
            return "p1";
        }else if(player1 == 'p' && player2 == 'r'){
            return "p1";
        }else if(player2 == 's' && player1 == 'p'){
            return "p2";
        }else if(player2 == 'r' && player1 == 's'){
            return "p2";
        }else if(player2 == 'p' && player1 == 'r'){
            return "p2";
        }
        return "draw";
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input player 1: (r,p,s)");
        char player1 = sc.next().charAt(0);
        System.out.println("Please input player 2: (r,p,s)");
        char player2 = sc.next().charAt(0);
        String winner = chup(player1, player2);
        if(winner.equals("draw")){
            System.out.println("Draw!!!");
        }else{
            System.out.println("The Winner is " + winner);
        }
    }
}
